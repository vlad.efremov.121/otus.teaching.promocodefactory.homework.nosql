﻿using System;
using System.Threading.Tasks;

namespace ReceivingFromPartner.Core.Abstractions.Gateways
{
    public interface INotificationGateway
    {
        Task SendNotificationToPartnerAsync(Guid partnerId, string message);
    }
}
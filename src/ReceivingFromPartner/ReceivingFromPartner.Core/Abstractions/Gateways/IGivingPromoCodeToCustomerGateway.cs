﻿using System.Threading.Tasks;
using ReceivingFromPartner.Core.Domain;

namespace ReceivingFromPartner.Core.Abstractions.Gateways
{
    public interface IGivingPromoCodeToCustomerGateway
    {
        Task GivePromoCodeToCustomer(PromoCode promoCode);
    }
}
﻿using System;
using System.Threading.Tasks;
using ReceivingFromPartner.Core.Abstractions.Gateways;

namespace ReceivingFromPartner.Integration
{
    public class NotificationGateway
        : INotificationGateway
    {
        public Task SendNotificationToPartnerAsync(Guid partnerId, string message)
        {
            //Код, который вызывает сервис отправки уведомлений партнеру
            
            return Task.CompletedTask;
        }
    }
}
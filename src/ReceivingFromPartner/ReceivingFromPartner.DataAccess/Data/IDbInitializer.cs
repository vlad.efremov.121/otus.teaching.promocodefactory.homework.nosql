﻿namespace ReceivingFromPartner.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}
﻿namespace Administration.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}
﻿namespace GivingToCustomer.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}